# CARP (Champalimaud Animal Recognition Project)

Dataset of CARP images, used to benchmark different algorithms of
tracking by identification. It was developed to improve the 
accumulation procedure of [idtracker.ai](http://idtracker.ai/). 

### Prerequisites

Some Python packages (see environment.yml for details). If you use
conda, you can run this to generate a conda environment with all the
prerrequisites:

`conda env create -f environment.yml`

### Installing

Just clone the repo and use pip

```
git clone XXX
cd carp
pip install .
```

The first time the code is run, a [8GB file](https://drive.google.com/uc?id=1d8amBB04j1bY58NwGbaKF-xvsEVymVn5) 
is downloaded automatically, by default to the temporal directory `/tmp`

There is a second, much smaller dataset that is used for testing.

## Examples

TODO: create a notebook

## Contributors

* **Francisco Romero-Ferrero**
* **Francisco J. H. Heras**

## License

This repository is free software (both as in freedom and as in free beer):
you can redistribute it and/or modify it under the terms of the GNU
General Public License as published by the Free Software Foundation,
either version 3 of the License, or (at your option) any later version.
In addition, the authors chose to distribute it free of charge by making it
publicly available. See the [license file](COPYING) for details


If you use this work in an academic context and you want to acknowledge us, 
please cite some of the relevant papers:

Romero-Ferrero, F., Bergomi, M. G., Hinz, R. C., Heras, F. J., & de Polavieja, G. G. (2019). idtracker.ai: tracking all individuals in small or large collectives of unmarked animals. Nature methods, 1

Heras, F. J., Romero-Ferrero, F., Hinz, R. C., & de Polavieja, G. G. (2019). Deep attention networks reveal the rules of collective motion in zebrafish. PLoS computational biology, 15(9), e1007354.

