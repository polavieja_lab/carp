from setuptools import setup

setup(
    name="carp",
    version="0.1",
    description="CARP library",
    url="http://gitlab.com/polavieja_lab/private/carp",
    author="Francisco J.H. Heras, Paco Romero-Ferrero",
    author_email="fjhheras@gmail.com",
    license="GPL",
    packages=["carp"],
    setup_requires=["pytest-runner"],
    test_require=["pytest"],
    zip_safe=False,
)
