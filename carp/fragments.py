"""Fragment carp module.

It contains the tools to fragment a time interval in different
``fragments'' separated by ``crossing fragments''

Interface:

    Fragments:
        begin_frames: inclusive (np.array of length num_fragments)
        end_frames: exclusive (np.array of length num_fragments)
        num_images (property): end_frames - begin_frames
        coexisting: (list of tuples, each tuple a couple of coexisting
          fragments)
        to_video_indices: maps indices from ``fragment coordinates''
          (fragment index, image in fragment) to ``CARP image
          coordinates'' (frame, individual)
        __len__ : number of fragments

    fragment_all: factory of CarpFragments

"""

from abc import ABC, abstractmethod
import copy
from typing import Dict, Iterator, List, Optional, Tuple

import numpy as np
from numpy.random import default_rng, Generator

DEFAULT_PARAMS = {
    "name": "independent_crossing_generator",
    "params": {
        "interval": {
            "name": "gamma",
            "kwargs": {"shape": 0.15, "scale": 250},
        },
        "duration": {
            "name": "constant",
            "kwargs": {"t": 3},
        },
    },
}


def coexisting_from_fragment_list(
    begin_frames: np.ndarray, end_frames: np.ndarray
) -> List[Tuple[int, int]]:
    """Given fragments, return the list of coexisting pairs"""
    edges = []
    for i, (begin_frame, end_frame) in enumerate(
        zip(begin_frames, end_frames)
    ):
        coexisting_with_i = np.where(
            np.logical_and(
                end_frames[:i] > begin_frame, begin_frames[:i] < end_frame
            )
        )[0]
        edges.extend([(j, i) for j in coexisting_with_i])
    return edges


def permute_labels(
    rng: Generator, arrays: List[np.ndarray]
) -> List[np.ndarray]:
    """
    Permute labels of fragments. Otherwise, labels are in order of identities,
    giving extra information that it is not in a real video
    """
    assert len({len(array) for array in arrays}) == 1
    permutation = rng.permutation(len(arrays[0]))
    return [array[permutation] for array in arrays]


def crossings_to_fragments_one_individual(
    crossings: List[Tuple], num_frames: int
) -> Tuple[List, List]:
    """
    Converts a list of tuples (begin_crossing, end_crossing) into a two lists:
        - begin_frames: starting frames of individual fragments
        - end_frames: end frames of individual fragments
    An individual fragment starts when a crossing ends and it ends when the
    next crossing starts
    Crossings are assumed to be non overlapping and trimmed to num_frames.
    To ensure this use the `merge_overlapping_crossings` and
    `trim_crossings` functions.
    """

    # Corner case, no crossings -> a single fragment along the video
    if not crossings:
        return [0], [num_frames]

    begin_crossing, end_crossing = zip(*crossings)
    begin_frames, end_frames = [], []
    for begin, end in zip(
        [0] + list(end_crossing), list(begin_crossing) + [num_frames]
    ):
        if end - begin > 0:
            begin_frames.append(begin)
            end_frames.append(end)

    return begin_frames, end_frames


def crossings_to_fragments(
    crossings: Dict[int, List], num_frames: int
) -> Tuple[np.ndarray, np.ndarray, np.ndarray]:
    """Compute the fragmens of all individuals from the crossings of
    all individuals"""
    all_identities = []
    all_begin_frames = []
    all_end_frames = []
    for i, crossings_i in crossings.items():
        begin_frames, end_frames = crossings_to_fragments_one_individual(
            crossings_i, num_frames
        )
        all_begin_frames.extend(begin_frames)
        all_end_frames.extend(end_frames)
        all_identities.extend([i] * len(begin_frames))

    return (
        np.asarray(all_identities),
        np.asarray(all_begin_frames),
        np.asarray(all_end_frames),
    )


def merge_overlapping_crossings(
    crossings: List[Tuple], inplace=True
) -> List[Tuple]:
    """Given a list of intervals ordered by starting point, it merges
    the overlapping intervals
    """
    # Corner case: empty list -> empty list
    if not crossings:
        return crossings

    merged_crossings = [crossings[0]]  # Initialize with first crossing
    for i in range(1, len(crossings)):
        if crossings[i][0] <= merged_crossings[-1][1]:
            # we extend the last crossing, as it overlaps with the new
            end = max(crossings[i][1], merged_crossings[-1][1])
            merged_crossings[-1] = (merged_crossings[-1][0], end)
        else:
            # new crossing that does not overlap with previous ones
            merged_crossings.append(crossings[i])
    if inplace:
        crossings[:] = merged_crossings
    return merged_crossings


def trim_crossings(
    crossings: List[Tuple], num_frames, inplace=True
) -> List[Tuple]:
    """Given a list of intervals, trims all of them to [0, num_frames]
    and discards empty intervals
    """
    trimmed_crossings = []
    for x in crossings:
        trimmed_start = max(0, min(num_frames, x[0]))
        trimmed_end = max(0, min(num_frames, x[1]))
        if trimmed_end > trimmed_start:
            trimmed_crossings.append((trimmed_start, trimmed_end))
    if inplace:
        crossings[:] = trimmed_crossings
    return trimmed_crossings


# Random number generators


def pseudo_binomial_ids(
    rng: Generator,
    num_individuals: int,
    p_crossing: float,
    animals_disappear: bool,
) -> Iterator[int]:
    min_indiv_crossing = 1 if animals_disappear else 2
    n = num_individuals - min_indiv_crossing
    p = p_crossing
    while True:
        num_ids = rng.binomial(n=n, p=p) + min_indiv_crossing
        yield rng.choice(num_individuals, num_ids, replace=False)


def bernoulli_1or2_ids(
    rng: Generator, num_individuals: int, p: float
) -> Iterator[int]:
    while True:
        num_ids = rng.binomial(n=1, p=p) + 1
        yield rng.choice(num_individuals, num_ids, replace=False)


def gamma(rng: Generator, **gamma_params) -> Iterator[int]:
    while True:
        yield int(np.ceil(rng.gamma(**gamma_params)))


def exponential_min_1_frames(rng: Generator, t: int) -> Iterator[int]:
    # Exponential distribution is a particular case of gamma
    return gamma(rng, scale=t, shape=1.0)


def constant(rng: Generator, t: int) -> Iterator[int]:
    while True:
        yield t


NUMBER_GENERATORS = {
    "bernoulli_1or2_ids": bernoulli_1or2_ids,
    "pseudo_binomial_ids": pseudo_binomial_ids,
    "exponential_min_1_frames": exponential_min_1_frames,
    "gamma": gamma,
    "constant": constant,
}

# Crossing generators


class CrossingGenerator(ABC):
    def __init__(
        self,
        rng: Generator,
        num_frames: int,
        num_individuals: int,
        params: Dict,
        warmingup_period: int = 100,
    ):
        self.params = copy.deepcopy(params)
        self.num_individuals = num_individuals
        self.num_frames = num_frames
        self.rng = rng
        self.warmingup_period = warmingup_period

        self.iterators = {}
        for generator_job, v in self.params.items():
            generator_name = v["name"]
            generator_kwargs = v["kwargs"]
            if "num_individuals" in generator_kwargs:
                if generator_kwargs["num_individuals"] is not None:
                    raise ValueError(
                        "num_individuals needs to be always"
                        " a placeholder (None)"
                    )
                # Completing info for num_individuals
                generator_kwargs["num_individuals"] = num_individuals

            # Storing used generators
            self.iterators[generator_job] = NUMBER_GENERATORS[generator_name](
                rng, **generator_kwargs
            )

    @abstractmethod
    def __call__(self) -> Dict[int, List[Tuple]]:
        pass

    def _merge_overlapping_and_trim(self, crossings: Dict[int, List]):
        for crossings_list in crossings.values():
            # crossings_list, and thus crossings, is modified in place
            merge_overlapping_crossings(crossings_list)
            trim_crossings(crossings_list, self.num_frames)


class ThreeStepCrossingGenerator(CrossingGenerator):
    """Samples crossing times and then samples which individuals
    participate in the crossing. This adds a dependency between
    crossing events of different individuals"""

    def __call__(self) -> Dict[int, List[Tuple]]:
        crossings = {i: [] for i in range(self.num_individuals)}
        start = end = -self.warmingup_period
        while end <= self.num_frames:
            # Crossing start computed from the start of the previous
            start += next(self.iterators["interval"])
            # Crossing end computed from the start
            end = start + next(self.iterators["duration"])
            crossing_ids = next(self.iterators["ids"])
            for id_ in crossing_ids:
                crossings[id_].append((start, end))
        self._merge_overlapping_and_trim(crossings)
        return crossings


class IndependentCrossingGenerator(CrossingGenerator):
    """For each individual independently, crossing times are sampled."""

    def _one_individual(self) -> List[Tuple]:
        crossings = []
        start = end = -self.warmingup_period
        while end <= self.num_frames:
            # Crossing start computed from the start of the previous
            start += next(self.iterators["interval"])
            # Crossing end computed from the start
            end = start + next(self.iterators["duration"])
            crossings.append((start, end))
        return crossings

    def __call__(self):
        crossings = {
            i: self._one_individual() for i in range(self.num_individuals)
        }
        self._merge_overlapping_and_trim(crossings)
        return crossings


CROSSING_GENERATORS = {
    "three_step_crossing_generator": ThreeStepCrossingGenerator,
    "independent_crossing_generator": IndependentCrossingGenerator,
}

# API


class Fragments:
    begin_frames: np.ndarray
    end_frames: np.ndarray
    coexisting: List[Tuple[int, int]]

    def __init__(
        self,
        identities: np.ndarray,
        begin_frames: np.ndarray,
        end_frames: np.ndarray,
        coexisting: Optional[List[Tuple[int, int]]] = None,
    ):
        """Fragments

        :param identities: Ground truth identities of the fragments
        :param begin_frames: Begin frames of the fragments. Begin frames are
        inclusive, i.e. if begin frame of fragment i is n, then fragment i
        contains n.
        :param end_frames: End frames of the fragments. End frames are
        exclusive, i.e. if end frame of fragment i is n, the fragment i does
        not contain n.
        :param coexisting: List of pairs of coexisting fragments. Fragment i
        and fragment j coexist and i < j iff (i, j) is on the list. If None
        (default) it is calculated from begin_frames and end_frames
        """

        # Number of individual fragments is the length of
        # identities, begin_frames and end_frames
        num_fragments = len(identities)
        assert num_fragments == len(begin_frames)
        assert num_fragments == len(end_frames)
        # No fragments without images
        assert (begin_frames < end_frames).all()
        # No fragments outside interval
        assert (0 <= begin_frames).all()

        if coexisting is None:
            self.coexisting = coexisting_from_fragment_list(
                begin_frames, end_frames
            )
        else:
            self.coexisting = coexisting

        # Asserting coexisting indices ordered and in range
        for v, w in self.coexisting:
            assert 0 <= v < w < num_fragments

        self._identities = identities
        self.begin_frames = begin_frames
        self.end_frames = end_frames

    def to_image_coordinates(
        self, fragment_indices: Tuple[int, int]
    ) -> Tuple[int, int]:
        """Transform fragment indices (fragment, index of image
        in fragment) to absolute indices in the video (frame,
        individual)
        """
        fragment_i, image_i = fragment_indices
        identity = self._identities[fragment_i]
        begin_frame = self.begin_frames[fragment_i]
        end_frame = self.end_frames[fragment_i]
        assert image_i < end_frame - begin_frame
        return begin_frame + image_i, identity

    @property
    def ground_truth(self):
        """Ground truth identities of the synthetic video.
        This is the solution of the problem, so DO NOT use
        in any of the solvers (only in testing)!"""
        return self._identities

    @property
    def num_images(self):
        """Number of images in each fragment"""
        return self.end_frames - self.begin_frames

    def __len__(self):
        return len(self._identities)


def fragment_all(
    num_frames: int,
    num_individual: int,
    params: Optional[Dict] = None,
    seed: int = 0,
) -> Fragments:
    """
    Generates fragments from a crossings generator.
    Given a dictionary "params" containing a string (key "name") and
    a dictionary (key "params") it returns a Fragments object.
    The string in the key "name" must be a key of CROSSING_GENERATORS
    """
    rng = default_rng(seed)

    if params is None:
        params = DEFAULT_PARAMS

    # First we generate a crossings dictionary with lists of crossings
    # (start, end) for each individual in the video
    crossing_generator = CROSSING_GENERATORS[params["name"]](
        rng, num_frames, num_individual, params["params"]
    )
    crossings = crossing_generator()

    # Then we convert the dictionary into a Fragments object
    identities, begin_frames, end_frames = crossings_to_fragments(
        crossings, num_frames
    )
    identities, begin_frames, end_frames = permute_labels(
        rng, [identities, begin_frames, end_frames]
    )
    return Fragments(
        identities=identities,
        begin_frames=begin_frames,
        end_frames=end_frames,
    )
