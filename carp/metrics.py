from typing import List, Optional, Union

import numpy as np
from scipy.optimize import linear_sum_assignment

# TODO: use numpy.typing when we use a higher version of numpy
# instead of Union[np.ndarray, List], etc


def accuracy_score(
    y_true: Union[np.ndarray, List],
    y_pred: Union[np.ndarray, List],
    importance: Optional[Union[np.ndarray, List]] = None,
) -> float:
    """Accuracy under assumption of symmetry under label exchange

    When importance is None (default), the output is the fraction
    of fragments with accurate identity predictions.

    When importance is the number of images in each fragment,
    the output is the fraction of images with accurate identity
    predictions.

    :param y_true: 1d array-like of ground-truth identities
    of the individual fragments
    :param y_pred: 1d array-like of predicted identities
    of the individual fragments
    :param importance: weight of each individual fragment in
    the accuracy
    """
    if len(y_true) != len(y_pred):
        raise ValueError(
            "input with different sizes ", len(y_true), len(y_pred)
        )

    y_true = np.array(y_true, dtype=int)
    y_pred = np.array(y_pred, dtype=int)
    num_fragments = len(y_true)
    if importance is None:
        importance = np.ones(num_fragments)
    else:
        importance = np.array(importance)
    # Assumptions:
    # 1. all identities from 0 to max are represented in y_true
    # 2. prediction has at most the same identities as g.t.
    num_identities = max(y_true) + 1
    score = np.zeros((num_identities, num_identities))
    for i in range(num_identities):
        for j in range(num_identities):
            # g.t. identity i/row, pred identity j/column
            common_fragments = np.logical_and(y_true == i, y_pred == j)
            score[i, j] = importance[common_fragments].sum()

    # Obtaining best identity matching using Hungarian algo
    true_ind, pred_ind = linear_sum_assignment(score, maximize=True)
    best_score = score[true_ind, pred_ind].sum()
    max_possible_score = importance.sum()
    return best_score / max_possible_score
