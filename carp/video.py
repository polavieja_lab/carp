"""Synthetic video CARP module.

Interface:

    FragmentedVideo:
        fragments (instance of Fragments)
        num_frames (property)
        num_individuals (property)
        __getitem__ : returns image at ``fragment coordinates''

    get_carp_fragmented_video: factory of FragmentedVideo

"""
from typing import Dict, Optional, Tuple

import numpy as np

from .fragments import Fragments, fragment_all
from .images import CarpImages, get_carp_images


class FragmentedVideo:
    """Main class of the CARP library. It contains a CarpImages
    instance to load images and a Fragments instance with info
    about the fragment distribution.
    """

    def __init__(self, images: CarpImages, fragments: Fragments):
        self._images = images
        self.fragments = fragments

    def __getitem__(self, key: Tuple[int, int]) -> np.ndarray:
        """Get an image in fragment coordinates

        :param key: (fragment index, image index in fragment)
        """
        absolute_frame, identity = self.fragments.to_image_coordinates(key)
        return self._images[absolute_frame, identity]

    @property
    def num_individuals(self) -> int:
        """Number of individuals in the synthetic video"""
        return self._images.num_individuals

    @property
    def num_frames(self) -> int:
        """Number of frames in the synthetic video"""
        return self._images.num_frames


def get_carp_fragmented_video(
    num_frames: int = 1000,
    num_individuals: int = 10,
    params: Optional[Dict] = None,
    seed: int = 0,
    root: str = "/tmp",
    download: bool = True,
) -> FragmentedVideo:
    images = get_carp_images(
        num_frames, num_individuals, root=root, download=download, seed=seed
    )
    fragments = fragment_all(
        num_frames, num_individuals, params=params, seed=seed
    )
    return FragmentedVideo(images, fragments)
