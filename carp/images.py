"""Images CARP module.

It contains the tools to: 1. Select a subset of individuals and a
interval of frames to generate a CarpImages instance
2. Access images from the generated CarpImages object using CARP image
coordinates (frame, individual)

Interface:

    CarpImages:
        num_frames (property)
        num_individuals (property)
        __getitem__ : returns image at ``CARP image coordinates''

    get_carp_images: factory of CarpImages

"""

import logging
import os
from typing import Tuple

import gdown
import h5py as h5
import numpy as np
from numpy.random import default_rng

logger = logging.getLogger(__name__)


class CarpImagesRaw:
    """Class with all images of 184 individuals and all frames in CARP"""

    _resources = [
        (
            "https://drive.google.com/uc?id=1d8amBB04j1bY58NwGbaKF-xvsEVymVn5",
            "CARP184_18819_52test.h5",
        )
    ]

    def __init__(self, root: str, download: bool = False):

        self.root = root

        if download:
            self.download()

        if not self._check_exists():
            raise RuntimeError(
                "Images not found."
                + " You can use download=True to download it"
            )

        raw_file = self._resources[0][1]
        self._data = h5.File(os.path.join(self.raw_folder, raw_file), "r")

    def _check_exists(self) -> bool:
        return all(
            os.path.isfile(os.path.join(self.raw_folder, raw_file))
            for _, raw_file in self._resources
        )

    @property
    def raw_folder(self) -> str:
        return os.path.join(self.root, "CARP_dataset", "raw")

    def download(self) -> None:
        "Download CARP data if it does not exist"

        if self._check_exists():
            logger.info("Did not download CARP, as it was already downloaded")
            return

        logger.info("Downloading CARP...")
        os.makedirs(self.raw_folder, exist_ok=True)
        for url, raw_file in self._resources:
            output_file = os.path.join(self.raw_folder, raw_file)
            gdown.download(url, output_file, quiet=False)

    def __getitem__(self, key: Tuple[int, int]) -> np.ndarray:
        return self._data["images"][key]

    @property
    def num_frames(self) -> int:
        return self._data["images"].shape[0]

    @property
    def num_individuals(self) -> int:
        return self._data["images"].shape[1]


class CarpImagesRawMock(CarpImagesRaw):
    """Dataset with few images. Only for testing purposes"""

    _resources = [
        (
            "https://drive.google.com/uc?id=1BAJMwR9YFZzM4DPI861j7f990XI3Q8h4",
            "CARPtest.h5",
        )
    ]


class CarpImages:
    """Class with a subset of individuals and an interval of frames"""

    def __init__(
        self,
        raw: CarpImagesRaw,
        individuals: np.ndarray = None,
        frames: np.ndarray = None,
    ):
        if individuals is None:
            individuals = np.arange(raw.num_individuals)
        if frames is None:
            frames = np.arange(raw.num_frames)

        assert (individuals < raw.num_individuals).all()
        assert (frames < raw.num_frames).all()

        self._raw = raw
        self._individuals = individuals
        self._frames = frames

    @property
    def num_individuals(self) -> int:
        return len(self._individuals)

    @property
    def num_frames(self) -> int:
        return len(self._frames)

    def __getitem__(self, key: Tuple[int, int]) -> np.ndarray:
        """Get an image in image coordinates

        :param key: (frame, individual)
        """
        frame, individual = key
        return self._raw[self._frames[frame], self._individuals[individual]]


def get_carp_images(
    num_frames: int,
    num_individuals: int,
    root: str = "/tmp",
    download: bool = True,
    seed: int = 0,
) -> CarpImages:

    # MOCK_CARP is an environment variable in the CI/CD that instructs test
    # to be performed only with the mock version of CARP
    if os.environ.get("MOCK_CARP", False):
        raw = CarpImagesRawMock(root=root, download=download)
    else:
        raw = CarpImagesRaw(root=root, download=download)

    if not (0 < num_individuals <= raw.num_individuals):
        raise ValueError(
            f"num_individuals must be between 1 and {raw.num_individuals}"
        )
    if not (0 < num_frames <= raw.num_frames):
        raise ValueError(f"num_frames must be between 1 and {raw.num_frames}")

    rng = default_rng(seed)
    individuals = rng.choice(
        raw.num_individuals, num_individuals, replace=False
    )

    frames_start = rng.choice(raw.num_frames - num_frames + 1)

    frames = np.arange(frames_start, frames_start + num_frames)
    return CarpImages(raw, individuals=individuals, frames=frames)
