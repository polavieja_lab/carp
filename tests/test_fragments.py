import numpy as np
from numpy.random import default_rng
import pytest

from carp.fragments import (
    IndependentCrossingGenerator,
    bernoulli_1or2_ids,
    coexisting_from_fragment_list,
    crossings_to_fragments,
    crossings_to_fragments_one_individual,
    exponential_min_1_frames,
    fragment_all,
    merge_overlapping_crossings,
    permute_labels,
    pseudo_binomial_ids,
    trim_crossings,
)


def assert_fragments_are_consistent(fragments, num_individuals):
    identities = fragments.ground_truth
    begin_frames = fragments.begin_frames
    end_frames = fragments.end_frames

    assert len(identities) == len(begin_frames) == len(end_frames)
    assert np.all(begin_frames < end_frames)
    assert np.all(identities < num_individuals)
    assert np.all(0 <= identities)


def assert_fragments_are_equal(fragments, fragments2):
    assert np.all(fragments2.begin_frames == fragments.begin_frames)
    assert np.all(fragments2.end_frames == fragments.end_frames)
    assert np.all(fragments2.ground_truth == fragments.ground_truth)
    assert np.all(
        np.array(fragments2.coexisting) == np.array(fragments.coexisting)
    )


params = [
    (100, 10, {"scale": 3, "shape": 3}, 4),
    (1000, 5, {"scale": 3, "shape": 3}, 10),
    (50, 3, {"scale": 10, "shape": 0.15}, 0),
]


@pytest.mark.parametrize(
    "num_frames,interval,duration",
    [(100, 1, 1), (100, 3, 2), (100, 4, 3)],
)
def test_fragment_one_individual(num_frames, interval, duration):
    rng = default_rng()
    params = {
        "interval": {"name": "constant", "kwargs": {"t": interval}},
        "duration": {
            "name": "constant",
            "kwargs": {"t": duration},
        },
    }

    x_generator = IndependentCrossingGenerator(rng, num_frames, 10, params)
    ind_crossings = x_generator._one_individual()
    merge_overlapping_crossings(ind_crossings)  # modified in place
    trim_crossings(ind_crossings, num_frames)  # modified in place
    begin_frames, end_frames = crossings_to_fragments_one_individual(
        ind_crossings, num_frames
    )
    ind_fragments = list(zip(begin_frames, end_frames))

    for fr1, fr2 in zip(ind_fragments[:-1], ind_fragments[1:]):
        if fr1[0] > 0:
            assert (fr2[0] - fr1[0]) == interval
        assert (fr2[0] - fr1[1]) == duration


crossing_generator_params = [
    {
        "name": "three_step_crossing_generator",
        "params": {
            "interval": {
                "name": "exponential_min_1_frames",
                "kwargs": {"t": 3},
            },
            "duration": {
                "name": "exponential_min_1_frames",
                "kwargs": {"t": 3},
            },
            "ids": {
                "name": "pseudo_binomial_ids",
                "kwargs": {
                    "num_individuals": None,
                    "p_crossing": 0.1,
                    "animals_disappear": False,
                },
            },
        },
    }
]

crossing_generator_params.append(crossing_generator_params[-1].copy())
crossing_generator_params[-1]["params"]["interval"]["kwargs"]["t"] = 10
crossing_generator_params[-1]["params"]["duration"]["kwargs"]["t"] = 5
crossing_generator_params[-1]["params"]["ids"]["kwargs"][
    "animals_disappear"
] = True

crossing_generator_params.append(crossing_generator_params[-1].copy())
crossing_generator_params[-1]["params"]["ids"] = {
    "name": "bernoulli_1or2_ids",
    "kwargs": {
        "num_individuals": None,  # updated in the code
        "p": 1.0,
    },
}

crossing_generator_params.append(crossing_generator_params[-1].copy())
crossing_generator_params[-1]["params"]["ids"]["kwargs"]["p"] = 0.0

crossing_generator_params.append(crossing_generator_params[-1].copy())
crossing_generator_params[-1]["params"]["ids"]["kwargs"]["p"] = 0.3

crossing_generator_params.append(
    {
        "name": "independent_crossing_generator",
        "params": {
            "interval": {
                "name": "gamma",
                "kwargs": {"shape": 0.5, "scale": 10},
            },
            "duration": {
                "name": "constant",
                "kwargs": {"t": 3},
            },
        },
    }
)

crossing_generator_params.append(crossing_generator_params[-1].copy())
crossing_generator_params[-1]["params"]["interval"]["kwargs"]["shape"] = 0.2


@pytest.mark.parametrize("params", crossing_generator_params)
@pytest.mark.parametrize("num_frames", [50, 100])
@pytest.mark.parametrize("num_individuals", [3, 5])
@pytest.mark.parametrize("seed", [0, 10])
def test_fragment_all(num_frames, num_individuals, params, seed):
    fragments = fragment_all(
        num_frames,
        num_individuals,
        params=params,
        seed=seed,
    )
    assert_fragments_are_consistent(fragments, num_individuals)


@pytest.mark.parametrize("params", crossing_generator_params)
@pytest.mark.parametrize("num_frames", [50, 100])
@pytest.mark.parametrize("num_individuals", [3, 5])
@pytest.mark.parametrize("seed", [0, 10])
def test_fragment_all_deterministic(num_frames, num_individuals, params, seed):
    fragments, fragments2 = [
        fragment_all(
            num_frames,
            num_individuals,
            params=params,
            seed=seed,
        )
        for _ in range(2)
    ]
    assert_fragments_are_equal(fragments, fragments2)


@pytest.mark.parametrize("params", crossing_generator_params)
@pytest.mark.parametrize("num_frames", [50, 100])
@pytest.mark.parametrize("num_individuals", [3, 5])
@pytest.mark.parametrize("seed", [0, 10])
def test_coexisting_from_fragment_list(
    num_frames, num_individuals, params, seed
):
    fragments = fragment_all(
        num_frames,
        num_individuals,
        params=params,
        seed=seed,
    )

    identities = fragments.ground_truth
    begin_frames = fragments.begin_frames
    end_frames = fragments.end_frames

    edges = coexisting_from_fragment_list(begin_frames, end_frames)
    for v2 in range(num_individuals):
        for v1 in range(num_individuals):
            coexisting = (v1, v2) in edges
            overlapping = bool(
                set(range(begin_frames[v1], end_frames[v1])).intersection(
                    set(range(begin_frames[v2], end_frames[v2]))
                )
            )
            # Coexisting if and only iff overlapping and different indices
            # Also, edges always go from lower to higher identity index
            assert coexisting == (overlapping and v1 < v2)
            # Two fragments of the same identity cannot coexist (except if
            # they have the same index)
            if coexisting and identities[v1] == identities[v2]:
                assert v1 == v2


cases = [
    ([(1, 4), (4, 6), (8, 9)], [(1, 6), (8, 9)]),
    ([(1, 4), (6, 8), (10, 12)], [(1, 4), (6, 8), (10, 12)]),
    ([(1, 4), (6, 8), (10, 14), (11, 13)], [(1, 4), (6, 8), (10, 14)]),
]


@pytest.mark.parametrize("crossings, expected", cases)
def test_merge_overlapping_crossings(crossings, expected):
    merge_overlapping_crossings(crossings)
    assert expected == crossings


cases = [
    ([(-2, 4), (5, 6), (8, 10)], 7, [(0, 4), (5, 6)]),
    ([(0, 4), (5, 6), (8, 10)], 8, [(0, 4), (5, 6)]),
    ([(1, 4), (5, 6), (8, 10)], 9, [(1, 4), (5, 6), (8, 9)]),
    ([(0, 4), (5, 6), (8, 10)], 10, [(0, 4), (5, 6), (8, 10)]),
    ([(-1, 4), (5, 6), (8, 10)], 11, [(0, 4), (5, 6), (8, 10)]),
]


@pytest.mark.parametrize("crossings, num_frames, expected", cases)
def test_trim_last_crossing(crossings, num_frames, expected):
    trim_crossings(crossings, num_frames)
    assert expected == crossings


cases = [
    ([(1, 4), (5, 6), (8, 10)], 8, [0, 4, 6], [1, 5, 8]),
    ([(1, 4), (5, 6), (8, 10)], 9, [0, 4, 6], [1, 5, 8]),
    ([(1, 4), (5, 6), (8, 10)], 10, [0, 4, 6], [1, 5, 8]),
    ([(1, 4), (5, 6), (8, 10)], 11, [0, 4, 6, 10], [1, 5, 8, 11]),
    ([(0, 4), (5, 6), (8, 10)], 8, [4, 6], [5, 8]),
    ([], 10, [0], [10]),
    ([(8, 10)], 9, [0], [8]),
]


@pytest.mark.parametrize(
    "crossings, num_frames, expected_begin_frames, expected_end_frames", cases
)
def test_crossings_to_fragments_one_individual(
    crossings, num_frames, expected_begin_frames, expected_end_frames
):
    begin_frames, end_frames = crossings_to_fragments_one_individual(
        crossings, num_frames
    )
    assert begin_frames == expected_begin_frames
    assert end_frames == expected_end_frames


cases = [
    [
        {
            0: [(1, 3), (5, 10), (13, 18)],
            1: [(1, 3), (13, 18)],
            2: [(5, 10)],
            3: [],
        },
        20,
        {
            "begin_frames": np.asarray([0, 3, 10, 18, 0, 3, 18, 0, 10, 0]),
            "end_frames": np.asarray([1, 5, 13, 20, 1, 13, 20, 5, 20, 20]),
            "identities": np.asarray([0, 0, 0, 0, 1, 1, 1, 2, 2, 3]),
        },
    ],
]


@pytest.mark.parametrize("crossings, num_frames, expected", cases)
def test_crossings_to_fragments(crossings, num_frames, expected):
    identities, begin_frames, end_frames = crossings_to_fragments(
        crossings, num_frames
    )
    assert all(identities == expected["identities"])
    assert all(begin_frames == expected["begin_frames"])
    assert all(end_frames == expected["end_frames"])


def test_permute_labels():
    rng = np.random.default_rng(0)
    arrays = [
        np.asarray([1, 2, 3]),
        np.asarray([3, 2, 1]),
        np.asarray([1, 1, 2]),
    ]
    permuted_arrays = permute_labels(rng, arrays)
    for array, permuted_array in zip(arrays, permuted_arrays):
        assert not all(array == permuted_array)


def test_exponential_min_1_frames():
    rng = np.random.default_rng(0)
    iter_exponential = exponential_min_1_frames(rng, t=10)
    samples = [next(iter_exponential) for _ in range(100)]
    assert all(sample >= 1 for sample in samples)


@pytest.mark.parametrize("animals_disappear", [False, True])
def test_pseudo_binomial_ids_generator(animals_disappear):
    rng = np.random.default_rng(0)
    ids_iterator = pseudo_binomial_ids(
        rng,
        num_individuals=10,
        p_crossing=0.1,
        animals_disappear=animals_disappear,
    )
    samples = [next(ids_iterator) for _ in range(100)]
    if animals_disappear:
        min_num_ids = 1
    else:
        min_num_ids = 2
    assert all(len(sample) >= min_num_ids for sample in samples)


@pytest.mark.parametrize("p", [0, 0.1, 0.3, 0.9, 1.0])
def test_bernoulli_1or2_ids_generator(p):
    rng = np.random.default_rng(0)
    ids_iterator = bernoulli_1or2_ids(rng, num_individuals=10, p=p)
    samples = [next(ids_iterator) for _ in range(100)]
    if p == 0:
        assert all(len(sample) == 1 for sample in samples)
    elif p == 1:
        assert all(len(sample) == 2 for sample in samples)
    else:
        assert all(len(sample) <= 2 for sample in samples)
