import pytest
import os

import carp.images
from carp.images import CarpImagesRaw, CarpImagesRawMock, get_carp_images

# If mock is true, the big CARP file is not downloaded and all
# tests are performed with a small mock CARP of <1MB
mock = os.environ.get("MOCK_CARP", False)

NUM_INDIVIDUALS_RAW = 184
NUM_FRAMES_RAW = 18819

NUM_INDIVIDUALS_RAW_MOCK = 3
NUM_FRAMES_RAW_MOCK = 50


@pytest.mark.skipif(mock, reason="MOCK_CARP in environment")
def test_carp_images_raw():
    images = CarpImagesRaw("/tmp", download=True)
    assert images.num_individuals == NUM_INDIVIDUALS_RAW
    assert images.num_frames == NUM_FRAMES_RAW


def test_carp_images_raw_mock():
    images = CarpImagesRawMock("/tmp", download=True)
    assert images.num_individuals == NUM_INDIVIDUALS_RAW_MOCK
    assert images.num_frames == NUM_FRAMES_RAW_MOCK


@pytest.mark.parametrize("num_frames,num_individuals", [(50, 3), (20, 2)])
def test_get_carp_images(num_frames, num_individuals):
    images = get_carp_images(num_frames, num_individuals)
    assert images.num_individuals == num_individuals
    assert images.num_frames == num_frames


@pytest.mark.parametrize(
    "num_frames,num_individuals,seed", [(20, 3, 5), (40, 2, 0)]
)
def test_get_carp_images_deterministic(num_frames, num_individuals, seed):
    images = get_carp_images(num_frames, num_individuals, seed=seed)
    images2 = get_carp_images(num_frames, num_individuals, seed=seed)
    for ind in range(num_individuals):
        for frame in range(num_frames):
            assert (images[frame, ind] == images2[frame, ind]).all()


@pytest.mark.parametrize("num_frames,extra_individuals", [(50, 1), (20, 2)])
def test_get_carp_images_too_many_individuals(num_frames, extra_individuals):
    if mock:
        num_individuals = extra_individuals + NUM_INDIVIDUALS_RAW_MOCK
    else:
        num_individuals = extra_individuals + NUM_INDIVIDUALS_RAW
    with pytest.raises(ValueError):
        get_carp_images(num_frames, num_individuals)


@pytest.mark.parametrize("extra_frames, num_individuals", [(5, 1), (2, 2)])
def test_get_carp_images_too_many_frames(extra_frames, num_individuals):
    if mock:
        num_frames = extra_frames + NUM_FRAMES_RAW_MOCK
    else:
        num_frames = extra_frames + NUM_FRAMES_RAW
    with pytest.raises(ValueError):
        get_carp_images(num_frames, num_individuals)
