import pytest

from carp.video import get_carp_fragmented_video

fragmentation_cases = [
    None,  # to test defaults
    {
        "name": "independent_crossing_generator",
        "params": {
            "interval": {
                "name": "gamma",
                "kwargs": {"shape": 0.1, "scale": 100},
            },
            "duration": {
                "name": "constant",
                "kwargs": {"t": 5},
            },
        },
    },
    {
        "name": "three_step_crossing_generator",
        "params": {
            "interval": {
                "name": "exponential_min_1_frames",
                "kwargs": {"t": 10},
            },
            "duration": {
                "name": "exponential_min_1_frames",
                "kwargs": {"t": 5},
            },
            "ids": {
                "name": "pseudo_binomial_ids",
                "kwargs": {
                    "num_individuals": None,  # updated in the code
                    "p_crossing": 0.1,
                    "animals_disappear": True,
                },
            },
        },
    },
]


@pytest.mark.parametrize("params", fragmentation_cases)
@pytest.mark.parametrize("num_frames", [40, 50])
@pytest.mark.parametrize("num_individuals", [2, 3])
def test_get_carp_fragmented_video(
    num_frames,
    num_individuals,
    params,
):
    video = get_carp_fragmented_video(
        num_frames,
        num_individuals,
        params,
    )
    assert video.num_individuals == num_individuals
    assert video.num_frames == num_frames
    assert len(video.fragments) >= num_individuals

    # Checking that API and directly calling _images produce
    # the same results
    for frag in range(len(video.fragments)):
        range_im_frag = range(
            video.fragments.begin_frames[frag],
            video.fragments.end_frames[frag],
        )
        for i, frame in enumerate(range_im_frag):
            gt_identity = video.fragments.ground_truth[frag]
            img_from_gt = video._images[frame, gt_identity]
            img_from_api = video[frag, i]
            assert (img_from_api == img_from_gt).all()
