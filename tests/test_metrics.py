import pytest

from carp.metrics import accuracy_score

accuracy_scores = [
    ([0, 1, 1, 1], [0, 1, 1, 1], 1.0),
    ([2, 0, 1], [1, 0, 2], 1.0),
    ([0, 1, 1], [0, 0, 1], 2 / 3),
    ([0, 1, 2, 3], [0, 0, 0, 0], 1 / 4),
    ([0, 1, 2, 0], [0, 0, 0, 0], 1 / 2),
]


@pytest.mark.parametrize("y_true, y_pred, true_score", accuracy_scores)
def test_accuracy_score(y_true, y_pred, true_score):
    assert accuracy_score(y_true, y_pred) == true_score


accuracy_scores_with_importance = [
    (y_true, y_pred, [2] * len(y_true), true_score)
    for (y_true, y_pred, true_score) in accuracy_scores
]

accuracy_scores_with_importance += [
    ([0, 1, 1, 1], [0, 1, 1, 1], [0, 1, 2, 1], 1.0),
    ([2, 0, 1], [1, 0, 2], [1, 0, 1], 1.0),
    ([0, 1, 1], [0, 0, 1], [1, 0, 1], 1.0),
    ([0, 1, 1], [0, 0, 1], [1, 1, 0], 1 / 2),
    ([0, 1, 2, 3], [0, 0, 0, 0], [1, 1, 1, 0], 1 / 3),
]


@pytest.mark.parametrize(
    "y_true, y_pred, importance, true_score", accuracy_scores_with_importance
)
def test_accuracy_score(y_true, y_pred, importance, true_score):
    assert accuracy_score(y_true, y_pred, importance=importance) == true_score
